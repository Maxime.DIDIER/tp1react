import { StatusBar } from 'expo-status-bar';
import {
  Alert,
  Button,
  StyleSheet,
  Text,
  View,
  TextInput,
  ImageBackground,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Modal,
  Keyboard, Pressable
} from 'react-native';
import TextInputReset from 'react-native-text-input-reset';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import * as React from "react";
import {Component, useState} from "react";
import {Touchable} from "react-native-web";
import alert from "react-native-web/dist/exports/Alert";
import keyboard from "react-native-web/dist/exports/Keyboard";
import resets from "react-native-web/dist/exports/StyleSheet/initialRules";



const sampleGoals = [
  {
    key: '1',
    title: "Faire les courses"
  },{
    key: '2',
    title: "Aller à la salle de sport 3 fois par semaine"
  },{
    key: '3',
    title: "Monter à plus de 5000m d altitude"
  },{
    key: '4',
    title:"Acheter mon premier appartement"
  },{
    key: '5',
    title: "Perdre 5 kgs"
  },{
    key: '6',
    title: "Gagner en productivité"
  },{
    key: '7',
    title: "Apprendre un nouveau langage"
  },{
    key: '8',
    title: "Faire une mission en freelance"
  },{
    key: '9',
    title: "Organiser un meetup autour de la tech"
  },
  {
    key: '10',
    title: "Faire un triathlon"
  }
];
export default function App() {
  const [name, setName] = React.useState("");
  const [liste, setListe] = React.useState(sampleGoals);
  const [input, setinput] = React.useState("");
  const [modalVisible, setModalVisible] = useState(false);

  const renderItem = ({item}) => (<Item title={item.title}/>);
  const onPress = () => {
    setListe([...liste, {key:Math.random().toString(), title: name}]);
    setModalVisible(false);
  }
  const deleteSelected = (key) => {
    const filterData = liste.filter(item => item.key !== key);
    setListe(filterData);
  }
  return (
      <ImageBackground source={require('./assets/mer.jpg')} resizeMode="cover" style={styles.image}>
        <KeyboardAwareView animated={true}>
        <SafeAreaView style={styles.container}>
        <FlatList data={liste} renderItem={({item}) => (<Text style={styles.item}>{item.title} |
            <View style={styles.test}>
            <TouchableOpacity onPress={() => deleteSelected(item.key)}>
              <Image source={require('./assets/supp.png')} style={styles.images} />
            </TouchableOpacity>
          </View>
        </Text>)}
          />
          <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
          >
            <View style={styles.view}>
              <View style={styles.view2}>
                <TextInput style={styles.textin} id="test56" placeholder="Write a task" clearButtonMode="always" onChangeText={(value) => setName(value)}/>
                  <Pressable style={styles.button} onPress={onPress}>
                    <Text style={styles.text}>
                      Add
                    </Text>
                  </Pressable>
              </View>

            </View>

          </Modal>
          <Pressable onPress={() => setModalVisible(true)}>
          <View style={styles.row}>
                <Text style={styles.textbutton}>Add</Text>
          </View>
          </Pressable>
        <StatusBar style="auto" />
        </SafeAreaView>
        </KeyboardAwareView>
  </ImageBackground>


  );
}

const styles = StyleSheet.create({
  view2: {
    top: '50%',
    flexDirection: "row",
  },
text: {
  color: "white",
  top: 20,
  textAlign: 'center'
},
  view: {
  width: '100%',
    height: '100%',
    backgroundColor: "white"
  },
  container: {
    flex: 1,
  },
  images: {
    width: '100%',
    height: '100%'
  },
  test2: {
    flexDirection: "row",
  },
  test: {
    flex: 1,
    backgroundColor: "white",
    borderRadius: 5,
    width: 50,
    height: 50
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  Text1: {
    color: 'red',
    fontSize: 20
  },
  Text2: {
    fontWeight: 'bold'
  },
  textin: {
    backgroundColor: "#fffafa",
    width: '65%',
    margin: 5,
    borderRadius: 7,
    borderWidth: 2,
    padding: 15,
  },
  row: {
    flexDirection: "row",
    backgroundColor: '#dc143c',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "white"
  },
  button: {
    backgroundColor: '#dc143c',
    height: '100%',
    width: '25%',
    borderRadius: 7,
    borderWidth: 2,
    borderColor: "white",

  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff7f50',
    marginVertical: 8,
    marginHorizontal: 16,
    flexDirection: 'row',
  },
  textbutton: {
    color: "white",
    textAlign: "center",
    fontSize: 20
  }
});
